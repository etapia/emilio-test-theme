# emilio-test-theme

Basic Theme for Shopify with the following modifications:
  * Use of the Shopify AJAX API to add products to the cart.
  * Change the appearance of the "Add to cart" status depending on the stock.
  * Addition of "Random Title" Theme Setting showed in the home page.
  * Addition of a Theme Section to list products from a specific collection.
  * Product page shows a modal with product stock and cart information.
